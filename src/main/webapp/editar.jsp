<%-- 
    Document   : index
    Created on : 08-abr-2020, 20:04:51
    Author     : javi3
--%>

<%@page import="cl.entities.Selecciones"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
  Selecciones sel=(Selecciones)request.getAttribute("selecciones");
 
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet"
         href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <title>Test Lista</title>
    </head>
    <body class="text-center" >
    <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <div class="inner">
    
        </div>
      </header>
    </div>
      <main role="main" class="inner cover">
        <h1 class="cover-heading">Actualizar Seleccion</h1>
        <p class="">
            
             <form  name="form" action="MantencionJugador" method="POST">
                    <div class="form-group">
                        <label for="codigo">codigo</label>
                        <input  name="codigo" value="<%= sel.getCodigo()%>" class="form-control" required id="codigo" aria-describedby="usernameHelp">
                           </div>
                    <br>
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input  step="any" name="nombre" value="<%= sel.getNombre()%>"  class="form-control" required id="nombre" aria-describedby="nombreHelp">
                     </div>       
                    <br>
                  
                    <br>
                    <button type="submit" name="accion" value="grabarEditar" class="btn btn-success">Grabar</button>
                     <button type="submit" name="accion" value="salirEditar"  class="btn btn-success">Salir</button>
                </form>
  
      </main>

  
      
       
    </body>
</html>