<%-- 
    Document   : index
    Created on : 08-abr-2020, 20:04:51
    Author     : javi3
--%>


<%@page import="cl.entities.Selecciones"%>
<%@page import="java.util.Iterator"%>

<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    List<Selecciones> usuarios = (List<Selecciones>) request.getAttribute("selecciones");
    Iterator<Selecciones> itJugadores = usuarios.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <title>Registro Clasificatorios Sudamericanas</title>
    </head>
    <body class="text-center" >
        <form name="form" action="MantencionJugador" method="POST">  
   
            <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
                <table border="1">
                    <thead>
                    <th>Rut</th>
                    <th>nombre </th>
             
                    <th> </th>
                    </thead>
                    <tbody>
                        <%while (itJugadores.hasNext()) {
                       Selecciones usu = itJugadores.next();%>
                        <tr>
                            <td><%= usu.getCodigo()%></td>
                            <td><%= usu.getNombre()%></td>
                          

                            <td> <input type="radio" name="seleccion" value="<%= usu.getCodigo()%>"> </td>
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>
                <button type="submit" name="accion" value="editar" class="btn btn-lg btn-secondary" style="margin-top: 20px;margin-bottom: 50px;">Editar</button>
                <button type="submit" name="accion" value="eliminar" class="btn btn-lg btn-secondary" style="margin-top: 20px;margin-bottom: 50px;">Eliminar</button>
                <button type="submit" name="accion"  value="inscribir" class="btn btn-lg btn-secondary" style="margin-top: 20px;margin-bottom: 50px;">inscribir</button>

        </form>
    </body>
</html>