/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.services;

import cl.dao.SeleccionesJpaController;
import cl.dao.exceptions.NonexistentEntityException;
import cl.entities.Selecciones;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/selecciones")
public class SeleccionesRest {



    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo() {

  
        SeleccionesJpaController dao = new SeleccionesJpaController();
        List<Selecciones> lista = dao.findSeleccionesEntities();
        System.out.println("lista.size():" + lista.size());
        return Response.ok(200).entity(lista).build();

    }
    
    
   @GET
   @Path("/{idbuscar}")
   @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("idbuscar") String idbuscar){
            SeleccionesJpaController dao =new SeleccionesJpaController();
     Selecciones  sel=dao.findSelecciones(idbuscar);
        return Response.ok(200).entity(sel).build();
    }
    
    
   @POST
   @Produces(MediaType.APPLICATION_JSON)
    public Response nuevo(Selecciones seleccion){
   System.out.println("nuevo seleccion.getNombre():" + seleccion.getNombre());
    System.out.println("nuevo seleccion.getCodigo():" + seleccion.getCodigo());     
   
   SeleccionesJpaController dao =new SeleccionesJpaController();
        try {
            dao.create(seleccion);
        } catch (Exception ex) {
            Logger.getLogger(SeleccionesRest.class.getName()).log(Level.SEVERE, null, ex);
            
        }
    return Response.ok(200).entity(seleccion).build();
     
    }
    
    @PUT
    public Response actualizar(Selecciones seleccion){
          SeleccionesJpaController dao =new SeleccionesJpaController();
        try {
            dao.edit(seleccion);
        } catch (Exception ex) {
            Logger.getLogger(SeleccionesRest.class.getName()).log(Level.SEVERE, null, ex);
        }
          
          
       return Response.ok(200).entity(seleccion).build();
     

    }
    
   @DELETE
   @Path("/{iddelete}")
   @Produces(MediaType.APPLICATION_JSON)
      public Response eliminaId(@PathParam("iddelete") String iddelete){
          System.out.println("eliminaId :" + iddelete);
            SeleccionesJpaController dao =new SeleccionesJpaController();
        try {
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(SeleccionesRest.class.getName()).log(Level.SEVERE, null, ex);
        }
       return Response.ok("cliente eliminado").build();
     

    }  
    
    
  
}
