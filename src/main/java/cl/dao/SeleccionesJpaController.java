/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.dao;

import cl.dao.exceptions.NonexistentEntityException;
import cl.dao.exceptions.PreexistingEntityException;
import cl.entities.Selecciones;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;


public class SeleccionesJpaController implements Serializable {

    
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("clientes_PU");
      
    public SeleccionesJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    
    
      public SeleccionesJpaController() {
      
    }  
    
   

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Selecciones selecciones) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(selecciones);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findSelecciones(selecciones.getCodigo()) != null) {
                throw new PreexistingEntityException("Selecciones " + selecciones + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Selecciones selecciones) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            selecciones = em.merge(selecciones);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = selecciones.getCodigo();
                if (findSelecciones(id) == null) {
                    throw new NonexistentEntityException("The selecciones with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Selecciones selecciones;
            try {
                selecciones = em.getReference(Selecciones.class, id);
                selecciones.getCodigo();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The selecciones with id " + id + " no longer exists.", enfe);
            }
            em.remove(selecciones);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Selecciones> findSeleccionesEntities() {
        return findSeleccionesEntities(true, -1, -1);
    }

    public List<Selecciones> findSeleccionesEntities(int maxResults, int firstResult) {
        return findSeleccionesEntities(false, maxResults, firstResult);
    }

    private List<Selecciones> findSeleccionesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Selecciones.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Selecciones findSelecciones(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Selecciones.class, id);
        } finally {
            em.close();
        }
    }

    public int getSeleccionesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Selecciones> rt = cq.from(Selecciones.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
