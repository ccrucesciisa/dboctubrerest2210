/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controller;

import cl.dto.IndicadorDto;
import cl.dto.ValorIndicador;
import cl.entities.Selecciones;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import utils.Config;

/**
 *
 * @author Ripley
 */
@WebServlet(name = "IngresoRegistroController", urlPatterns = {"/IngresoRegistroController"})
public class IngresoRegistroController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet IngresoRegistroController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet IngresoRegistroController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String boton = request.getParameter("accion");

        if (boton.equals("registrar")) {

            Config.getVariablesEntorno();
            
            Client client = ClientBuilder.newClient();
            WebTarget myResource = client.target(Config.host);

            List<Selecciones> sel = myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<List<Selecciones>>() {
            });

            System.out.println("sel.size():" + sel.size());
            request.setAttribute("selecciones", sel);
            request.getRequestDispatcher("registro.jsp").forward(request, response);

        } else {
            Client client = ClientBuilder.newClient();
            WebTarget myResource = client.target("https://mindicador.cl/api/uf/20-10-2020");

            IndicadorDto indicador = myResource.request(MediaType.APPLICATION_JSON).get(IndicadorDto.class);

            System.out.println("indicador.getCodigo()" + indicador.getCodigo());
            System.out.println("indicador.getSerie().size()" + indicador.getSerie().size());
            
            
            ValorIndicador valor=indicador.getSerie().get(0);
            request.setAttribute("valor", valor);
            request.getRequestDispatcher("indicadores.jsp").forward(request, response);

        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
