/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controller;

import cl.entities.Selecciones;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import utils.Config;

/**
 *
 * @author Ripley
 */
@WebServlet(name = "MantencionJugador", urlPatterns = {"/MantencionJugador"})
public class MantencionJugador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MantencionJugador</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MantencionJugador333 at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("doPost 1");
        String accion = request.getParameter("accion");

        if (accion.equals("inscribir")) {

            request.getRequestDispatcher("inscripcion.jsp").forward(request, response);
        }

        if (accion.equals("inscribirgrabar")) {

            String codigo = request.getParameter("codigo");
            String nombre = request.getParameter("nombre");

            System.out.println("rut" + codigo);
            System.out.println("nombre" + nombre);

            Selecciones selaCrear = new Selecciones();
            selaCrear.setCodigo(codigo);
            selaCrear.setNombre(nombre);

            Client client1 = ClientBuilder.newClient();
            Config.getVariablesEntorno();
            WebTarget myResource1 = client1.target(Config.host);
            myResource1.request(MediaType.APPLICATION_JSON).post(Entity.json(selaCrear), Selecciones.class);

            Client client = ClientBuilder.newClient();
            
            WebTarget myResource = client.target(Config.host);

            List<Selecciones> sel = myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<List<Selecciones>>() {
            });

            System.out.println("sel.size():" + sel.size());
            request.setAttribute("selecciones", sel);
            request.getRequestDispatcher("registro.jsp").forward(request, response);

        }

        if (accion.equals("eliminar")) {

            String seleccion = request.getParameter("seleccion");
            System.out.println("se eliminar codigo:" + seleccion);
            Client client1 = ClientBuilder.newClient();
            Config.getVariablesEntorno();

            WebTarget myResource1 = client1.target(Config.host+"/" + seleccion);
            myResource1.request(MediaType.APPLICATION_JSON).delete();

            Client client = ClientBuilder.newClient();
            WebTarget myResource = client.target(Config.host);

            List<Selecciones> sel = myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<List<Selecciones>>() {
            });

            System.out.println("sel.size():" + sel.size());
            request.setAttribute("selecciones", sel);
            request.getRequestDispatcher("registro.jsp").forward(request, response);

        }
        if (accion.equals("editar")) {
            String seleccion = request.getParameter("seleccion");

            Client client = ClientBuilder.newClient();
            Config.getVariablesEntorno();
           // WebTarget myResource = client.target("http://LAPTOP-TFF4I6NM:8081/dboctubrerest-1.0-SNAPSHOT/api/selecciones/" + seleccion);
             WebTarget myResource = client.target(Config.host+"/" + seleccion);
            Selecciones sel = myResource.request(MediaType.APPLICATION_JSON).get(Selecciones.class);

            System.out.println("sel.getNombre():" + sel.getNombre());
            request.setAttribute("selecciones", sel);
            request.getRequestDispatcher("editar.jsp").forward(request, response);

        }
        if (accion.equals("grabarEditar")) {

            String codigo = request.getParameter("codigo");
            String nombre = request.getParameter("nombre");

            System.out.println("rut" + codigo);
            System.out.println("nombre" + nombre);

            Selecciones selaEditar = new Selecciones();
            selaEditar.setCodigo(codigo);
            selaEditar.setNombre(nombre);

            Client client1 = ClientBuilder.newClient();
            WebTarget myResource1 = client1.target("http://LAPTOP-TFF4I6NM:8081/dboctubrerest-1.0-SNAPSHOT/api/selecciones");
            myResource1.request(MediaType.APPLICATION_JSON).put(Entity.json(selaEditar), Selecciones.class);

            Client client = ClientBuilder.newClient();
            WebTarget myResource = client.target("http://LAPTOP-TFF4I6NM:8081/dboctubrerest-1.0-SNAPSHOT/api/selecciones");

            List<Selecciones> sel = myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<List<Selecciones>>() {
            });

            System.out.println("sel.size():" + sel.size());
            request.setAttribute("selecciones", sel);
            request.getRequestDispatcher("registro.jsp").forward(request, response);

        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
